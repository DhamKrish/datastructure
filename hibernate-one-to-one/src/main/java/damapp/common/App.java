package damapp.common;



import java.sql.Date;

import org.hibernate.Session;

import damapp.domain.Stock;
import damapp.domain.StockDetail;
import damapp.persistence.HibernateUtil;

public class App 
{
    public static void main( String[] args )
    {
		System.out.println("Hibernate one to one (XML mapping)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();

		stock.setStockCode("6000");
		stock.setStockName("k006");

		StockDetail stockDetail = new StockDetail();
		stockDetail.setCompName("GENTING Malaysia");
		stockDetail.setCompDesc("Best resort in the world");
		stockDetail.setRemark("Nothing Special");
		stockDetail.setListedDate(new Date(System.currentTimeMillis()));

		stock.setStockDetail(stockDetail);
		stockDetail.setStock(stock);

		session.save(stock);
		session.getTransaction().commit();

		System.out.println("Done");
        
        HibernateUtil.shutdown();
    }
}