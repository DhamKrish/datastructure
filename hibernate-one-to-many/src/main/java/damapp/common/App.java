package damapp.common;



import java.sql.Date;

import org.hibernate.Session;

import damapp.domain.Stock;
import damapp.domain.StockDailyRecord;
import damapp.domain.StockDetail;
import damapp.persistence.HibernateUtil;

public class App 
{
    public static void main( String[] args )
    {
		System.out.println("Hibernate one to one (XML mapping)");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();

		Stock stock = new Stock();
        stock.setStockCode("7500");
        stock.setStockName("YaazhGuitar");
//        session.save(stock);
        
		StockDetail stockDetail = new StockDetail();
		stockDetail.setCompName("YaazhStore India");
		stockDetail.setCompDesc("Best resort in the world");
		stockDetail.setRemark("Nothing Special");
		stockDetail.setListedDate(new Date(System.currentTimeMillis()));

		stock.setStockDetail(stockDetail);
		stockDetail.setStock(stock);

		session.save(stock);

        StockDailyRecord stockDailyRecords = new StockDailyRecord();
        stockDailyRecords.setPriceOpen(new Float("1.2"));
        stockDailyRecords.setPriceClose(new Float("1.1"));
        stockDailyRecords.setPriceChange(new Float("10.0"));
        stockDailyRecords.setVolume(3000000L);
        stockDailyRecords.setDate(new Date(System.currentTimeMillis()));
        
        stockDailyRecords.setStock(stock);        
        stock.getStockDailyRecords().add(stockDailyRecords);

        StockDailyRecord stockDailyRecords2 = new StockDailyRecord();
        stockDailyRecords2.setPriceOpen(new Float("2.2"));
        stockDailyRecords2.setPriceClose(new Float("2.1"));
        stockDailyRecords2.setPriceChange(new Float("20.0"));
        stockDailyRecords2.setVolume(6000000L);
        stockDailyRecords2.setDate(new Date(System.currentTimeMillis()));
        
        stockDailyRecords2.setStock(stock);        
        stock.getStockDailyRecords().add(stockDailyRecords2);
        
        session.save(stockDailyRecords);
        session.getTransaction().commit();

		System.out.println("Done");
        
        HibernateUtil.shutdown();
    }
}