package naveen.service;

import java.util.List;

import naveen.domain.Bike;
import naveen.web.BikeInfo;

/**
 * @author dhamo
 * 
 */
public interface BikeManager {

	public List<Bike> getBikeList();

	public void saveBikeInfo(BikeInfo bikeInfo);

	public BikeInfo getBike(int BikeNum);

	public void deleteBike(int BikeNum);

}