package naveen.service;

import java.util.List;

import naveen.domain.Bike;
import naveen.repository.BikeDao;
import naveen.web.BikeInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("bikeManager")
public class BikeManagerService implements BikeManager {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private BikeDao bikeDao;

	public List<Bike> getBikeList() {
		return bikeDao.getBikeList();
	}

	public void saveBikeInfo(BikeInfo bikeInfo) {
		Bike bike = new Bike();
		bike.setBikeNum(bikeInfo.getbikeNum());
		bike.setName(bikeInfo.getName());
		bike.setYear(bikeInfo.getYear());
		// TODO: To check if single char is supplied. F-Female, M-Male.
		// student.setSex(studentInfo.getSex().charAt(0));
		bike.setColor(bikeInfo.getColor());
		bike.setCountry(bikeInfo.getCountry());

		logger.info("bike save:" + bike);

		if ("edit".equals(bikeInfo.getMode())) {
			bikeDao.updateBike(bike);
		} else {
			bikeDao.saveBike(bike);
		}
	}

	public BikeInfo getBike(int Num) {
		Bike bike = bikeDao.getBike(Num);
		BikeInfo bikeInfo = new BikeInfo();
		bikeInfo.setYear(bike.getYear());
		bikeInfo.setCountry(bike.getCountry());
		bikeInfo.setColor(bike.getColor());
		bikeInfo.setBikeNum(bike.getBikeNum());
		// bikeInfo.setSex(String.valueOf(student.getSex()));
		bikeInfo.setName(bike.getName());
		return bikeInfo;

	}

	public void deleteBike(int bikeNum) {
		bikeDao.deleteBike(bikeNum);
	}
}