package naveen.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import naveen.domain.Bike;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.stereotype.Repository;

@Repository("bikeDao")
public class JdbcBikeDao implements BikeDao {

	/** Logger for this class and subclasses */
	protected final Log logger = LogFactory.getLog(getClass());

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}

	/**
	 * @author dhamo
	 * 
	 */
	private static class BikeMapper implements ParameterizedRowMapper<Bike> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow
		 * (java.sql.ResultSet, int)
		 */
		public Bike mapRow(ResultSet resultSet, int arg1) throws SQLException {

			Bike bike = new Bike();
			bike.setBikeNum(resultSet.getInt("BIKENUM"));
			bike.setName(resultSet.getString("NAME"));
			bike.setYear(resultSet.getInt("YEAR"));
			// student.setSex(resultSet.getString("SEX").charAt(0));
			bike.setColor(resultSet.getString("COLOR"));
			bike.setCountry(resultSet.getString("COUNTRY"));
			return bike;
		}

	}

	public List<Bike> getBikeList() {
		logger.info("Getting Bike");

		List<Bike> bikeList = getJdbcTemplate().query(
				"SELECT BIKENUM, NAME, YEAR, COLOR, COUNTRY FROM BIKE_DETAILS WHERE IS_ACTIVE='Y'",
				new BikeMapper());
		logger.info("Bike List" + bikeList);
		return bikeList;
	}

	public void saveBike(Bike bike) {

		logger.info("Saving Bike:" + bike.getName());

		Object updObj[] = new Object[] { 
				bike.getBikeNum(), 
				bike.getName(),
				bike.getYear(), 
				bike.getColor(), 
				bike.getCountry() };
		int updArgTypes[] = new int[] { 
				Types.INTEGER, // bikeNum 
				Types.VARCHAR, //name
				Types.INTEGER,  //year
				Types.VARCHAR, //color
				Types.VARCHAR //country
				};
		String updStmt = "INSERT INTO BIKE_DETAILS(BIKENUM, NAME, YEAR, COLOR, COUNTRY) VALUES(?, ?, ?, ?, ?)";
     
		int updCount = this.getJdbcTemplate().update(updStmt, updObj,
				updArgTypes);
		logger.info("Rows affected :" + updCount);
	}           

	public Bike getBike(int bikeNum) {
		List<Bike> bikeList = getJdbcTemplate().query(
				"SELECT BIKENUM, NAME, YEAR, COLOR, COUNTRY FROM BIKE_DETAILS WHERE BIKENUM="
						+ bikeNum, new BikeMapper());
		logger.info("Bike List" + bikeList);
		return bikeList.get(0);
	}

	public void updateBike(Bike bike) {
		logger.info("Updating Bike:" + bike.getName());

		Object updObj[] = new Object[] { bike.getYear(), bike.getColor(),
				bike.getCountry(), bike.getBikeNum() };
		int updArgTypes[] = new int[] { Types.INTEGER, Types.VARCHAR,
				Types.VARCHAR, Types.INTEGER };
		String updStmt = "UPDATE BIKE_DETAILS " 
				+ " SET YEAR=?," 
				+ " COLOR=?, "
				+ " COUNTRY =? " 
				+ " WHERE BIKENUM=? ";

		int updCount = this.getJdbcTemplate().update(updStmt, updObj,
				updArgTypes);
		logger.info("Update Bike - Rows affected :" + updCount);

	}

	public void deleteBike(int bikeNum) {
		logger.info("Deleting Bike:" + bikeNum);

		Object updObj[] = new Object[] { bikeNum };
		int updArgTypes[] = new int[] { Types.INTEGER };
		String updStmt = "UPDATE BIKE_DETAILS " + " SET IS_ACTIVE='N' "
				+ " WHERE BIKENUM=? ";

		int updCount = this.getJdbcTemplate().update(updStmt, updObj,
				updArgTypes);
		logger.info("Update BIKE - Rows affected :" + updCount);

	}
}