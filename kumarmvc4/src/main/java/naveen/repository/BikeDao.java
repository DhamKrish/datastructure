package naveen.repository;

import java.util.List;

import naveen.domain.Bike;

public interface BikeDao {

	public List<Bike> getBikeList();

	public void saveBike(Bike bike);

	public Bike getBike(int BikeNum);

	public void updateBike(Bike bike);

	public void deleteBike(int BikeNum);

}
