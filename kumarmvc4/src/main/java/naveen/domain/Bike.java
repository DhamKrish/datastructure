package naveen.domain;

import java.io.Serializable;

public class Bike implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer bikeNum;
	private String name;
	private Integer year;
	private String color;
	private String country;

	
	public Integer getBikeNum() {
		return bikeNum;
	}

	public void setBikeNum(Integer bikeNum) {
		this.bikeNum = bikeNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Bike [bikeNum=" + bikeNum + ", name=" + name + ", year=" + year
				+ ", color=" + color + ", country=" + country + "]";
	}
}