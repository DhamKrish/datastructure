package naveen.web;

import java.io.Serializable;

import javax.validation.constraints.Size;

public class BikeInfo implements Serializable {

	private int bikeNum;

	@Size(min = 5, max = 50)
	private String Name;
	private int Year;
	private String Color;
	private String country;

	/**
	 * mode = add, edit 
	 */
	private String mode;	

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public int getbikeNum() {
		return bikeNum;
	}

	public void setBikeNum(int bikenum) {
		bikeNum = bikenum;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getYear() {
		return Year;
	}

	public void setYear(int year) {
		Year = year;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "BikeInfo [bikeNum=" + bikeNum + ", Name=" + Name + ", Year="
				+ Year + ", Color=" + Color + ", country=" + country
				+ ", mode=" + mode + "]";
	}

}