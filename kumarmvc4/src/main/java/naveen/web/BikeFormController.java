package naveen.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.validation.Valid;

import naveen.service.BikeManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author dhamo StudentFormController - add and list methods.
 */
@Controller
@RequestMapping("/bike")
public class BikeFormController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private BikeManager bikeManager;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView getBikeList() throws ServletException, IOException {

		logger.info("Displaying Bike list...");
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("bikeList", this.bikeManager.getBikeList());
		return new ModelAndView("bikelist", "model", myModel);
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addBike(ModelMap myModel) {
		logger.info("Calling add bike");
		myModel.addAttribute("bikeInfo", new BikeInfo());
		return "bikeForm";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String editBike(@RequestParam int bikeNum, ModelMap myModel) {
		logger.info("Calling edit bike for :" + bikeNum);
		BikeInfo editBike = this.bikeManager.getBike(bikeNum);
		editBike.setMode("edit");
		myModel.addAttribute("bikeInfo", editBike);

		return "bikeForm";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteBike(@RequestParam int bikeNum) {
		logger.info("Calling delete bike for :" + bikeNum);
		this.bikeManager.deleteBike(bikeNum);

		return new ModelAndView("redirect:list");
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveBike(@Valid BikeInfo bikeInfo) {
		logger.info("Calling save bike - Mode:" + bikeInfo.getMode());
		this.bikeManager.saveBikeInfo(bikeInfo);

		return new ModelAndView("redirect:list");
	}

	/*
	 * Method used to populate the country list in view. Note that here you can
	 * call external systems to provide real data.
	 */
	@ModelAttribute("countryMap")
	public List<String> initializeCountries() {

		List<String> countries = new ArrayList<String>();
		countries.add("USA");
		countries.add("CANADA");
		countries.add("FRANCE");
		countries.add("GERMANY");
		countries.add("ITALY");
		countries.add("OTHER");
		return countries;
	}

	/*
	 * Method used to populate the country list in view. Note that here you can
	 * call external systems to provide real data.
	 */
	@ModelAttribute("colorMap")
	public List<String> initializeGrades() {

		List<String> colorsList = new ArrayList<String>();
		colorsList.add("BLUE");
		colorsList.add("RED");
		return colorsList;
	}

	// gradeMap
}