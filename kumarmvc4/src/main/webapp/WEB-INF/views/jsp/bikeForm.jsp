<%@ include file="/WEB-INF/views/jsp/include.jsp"%>

<html>
<head>
<title>Add Bikes</title>
<script>
function saveBike(){
 with(document.forms[0]){
   action = APP_CONTEXT + "/bike/save";
   method = "POST";
   submit();
 }
}
</script>
</head>
<body>
	<h3>Add Bikes</h3>
	<form:form id="addBikeForm" cssClass="form-horizontal"
		modelAttribute="bikeInfo" method="post" >
		<table width="95%" bgcolor="f8f8ff" border="0" cellspacing="0" cellpadding="5">
			
			<tr>
				<td align="right" width="20%">bike num:</td>
				<td width="*">
			
				<form:hidden path="mode"/>
			<c:if test = "${mode eq 'edit'}">
				<form:input path="bikeNum" readonly="true" /></td>
			</c:if>
			<c:if test="${mode != 'edit'}">
				<form:input path="bikeNum" /></td>
			</c:if>
			
			</tr>
			<tr>
				<td align="right" width="20%">Name:</td>
				<td width="*"><form:input path="Name" /></td>
			</tr>
			<tr>
				<td align="right" width="20%">Year:</td>
					<td width="*"><form:input path="Year" /></td>
			</tr>
			
			<tr>
				<td align="right" width="20%">Color:</td>
				<td width="*"><form:select path="color" items="${colorMap}"
						multiple="true" /></td>
			</tr>
			<tr>
				<td align="right" width="20%">Country:</td>
				<td width="*"><form:select path="country" items="${countryMap}" />
				</td>
			</tr>
		</table>
		<br>
		<input type="button" align="center" value="Execute" onclick="javascript:saveBike()">
	

	</form:form>
</body>

</html>