<%@ include file="/WEB-INF/views/jsp/include.jsp"%>

<html>
<head>
<title>Bikes</title>
<style>
.editIcon{
  background: url(edit.png) no-repeat 6px center;
  width: 50px;
  height: 50px;
  display: block;
}
.deleteIcon{
  background: url(delete.jpg) no-repeat 6px center;
  width: 10px;
  height: 10px;
  display: block;
}
</style>
</head>
<body>
	<h3>Bikes</h3>
	<form:form id="bikeListForm" cssClass="form-horizontal" modelAttribute="model" method="post" action="/kumarmvc4/bike/list">
		<TABLE border="1" cellspacing="1" cellpadding="1" bgColor="pink">
			<TR>
				<TH>BikeNum</TH>
				<TH>Name</TH>
				<TH>Year</TH>
				<TH>Color</TH>
				<TH>Country</TH>
				<TH>Edit</TH>
				<TH>Delete</TH>
			</TR>
			
			<c:forEach items="${model.bikeList}" var="bike">
				<TR>
					<TD> <c:out value="${bike.bikeNum}" /> </TD>
					<TD> <c:out value="${bike.name}" /> </TD>
					<TD> <c:out value="${bike.year}" /> </TD>
					<TD> <c:out value="${bike.color}" /> </TD>
					<TD> <c:out value="${bike.country}" /> </TD>
					<TD><a href="edit?bikeNum=${bike.bikeNum}" class="editIcon" title="Edit"></a></TD>
					<TD><a href="delete?bikeNum=${bike.bikeNum}" class="deleteIcon" title="Delete"></a></TD>
				</TR>
			</c:forEach>
		</TABLE>
		<a href="<c:url value="/bike/add"/>">Add Bike</a>
		</form:form>
</body>

</html> 