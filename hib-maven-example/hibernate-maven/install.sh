echo "running maven clean , hbm2cfgxml, hbm2hbmxml"
mvn clean hibernate3:hbm2cfgxml hibernate3:hbm2hbmxml
echo "copying views to target"
#rem cp docs/*.hbm.xml target/generated-mappings/com/fleetstudio/puma/mbbq/model
echo "running maven hbm2java, hbm2doc, install"
mvn hibernate3:hbm2java hibernate3:hbm2doc source:jar install
