package kar.domain;

public class GenericType<T> {

	private T t;

	public T getT() {
		return t;
	}

	public void setT(T t) {
		this.t = t;
	}
	
	
	   @Override
	public String toString() {
		return "GenericType [t=" + t + "]";
	}

	public static void main(String args[]){
	        GenericType<String> type = new GenericType<String>();
	        type.setT("Pankaj"); //valid
	         
	       System.out.println(type.t);
	   }
}