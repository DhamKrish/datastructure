package naveenjdbcconnection;

//STEP 1. Import required packages
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Jdbcexample {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/INFORMATION_SCHEMA";

	// Database credentials
	static final String USER = "root";
	static final String PASS = "root";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		
		try{
      //STEP 2: Register JDBC driver
      Class.forName("com.mysql.jdbc.Driver");

      //STEP 3: Open a connection
      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL,USER,PASS);

      //STEP 4: Execute a query
      System.out.println("Creating statement...");
      stmt = conn.createStatement();
      String sql;
      sql = (" SELECT TABLE_NAME, COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_TABLE_NAME, REFERENCED_COLUMN_NAME "
             + " FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE " 
             + " WHERE REFERENCED_COLUMN_NAME = 'emp_id' "
             + " AND REFERENCED_TABLE_NAME = 't_employees' ");
      			
      ResultSet rs = stmt.executeQuery(sql);

      List<ReferenceData> referenceDataList =new ArrayList<ReferenceData>();
      
      //STEP 5: Extract data from result set
      while(rs.next()){
         //Retrieve by column name
    	  
    	  ReferenceData referenceData = new ReferenceData();
    	  
    	  referenceData.setTablename(rs.getString("TABLE_NAME"));
    	  referenceData.setColumname(rs.getString("COLUMN_NAME"));
    	  referenceData.setColumname(rs.getString("CONSTRAINT_NAME"));
    	  referenceData.setReferencedtablename(rs.getString("REFERENCED_TABLE_NAME"));
    	  referenceData.setReferencedcolumnname(rs.getString("REFERENCED_COLUMN_NAME"));
    	  
    	  referenceDataList.add(referenceData);
      }
      
      System.out.println(referenceDataList);
    	  
  /*       String table_name  = rs.getString("TABLE_NAME");
         String column_name = rs.getString("COLUMN_NAME");
         String constrains_name = rs.getString("CONSTRAINT_NAME");
         String referencedtable_name = rs.getString("REFERENCED_TABLE_NAME");
         String referencedcolumn_name = rs.getString("REFERENCED_COLUMN_NAME");

   
         System.out.print("TABLENAME: " + table_name);
//         String column_name = null;
	   	 System.out.println(", COLUMNNAME : " + column_name);
         System.out.println(", CONSTRAINSNAME : " + constrains_name);
         System.out.println(", REFERENCEDTABLENAME : " + referencedtable_name);
         System.out.println(", REFERENCEDCOLUMNNAME : " + referencedcolumn_name);
*/  
      //STEP 6: Clean-up environment
      rs.close();
      stmt.close();
      conn.close();
   }catch(SQLException se){
      //Handle errors for JDBC
      se.printStackTrace();
   }catch(Exception e){
      //Handle errors for Class.forName
      e.printStackTrace();
   }finally{
      //finally block used to close resources
      try{
         if(stmt!=null)
            stmt.close();
      }catch(SQLException se2){
      }
      try{
         if(conn!=null)
            conn.close();
      }catch(SQLException se){
         se.printStackTrace();
      }
   }
   System.out.println("Goodbye!");
}

}