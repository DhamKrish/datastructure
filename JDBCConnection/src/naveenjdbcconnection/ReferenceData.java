package naveenjdbcconnection;

public class ReferenceData {
	String tablename;
	String columname;
	String constraintname;
	String referencedtablename;
	String referencedcolumnname;
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
	public String getColumname() {
		return columname;
	}
	public void setColumname(String columname) {
		this.columname = columname;
	}
	public String getConstraintname() {
		return constraintname;
	}
	public void setConstraintname(String constraintname) {
		this.constraintname = constraintname;
	}
	public String getReferencedtablename() {
		return referencedtablename;
	}
	public void setReferencedtablename(String referencedtable) {
		this.referencedtablename = referencedtable;
	}
	public String getReferencedcolumnname() {
		return referencedcolumnname;
	}
	public void setReferencedcolumnname(String referencedcolumn) {
		this.referencedcolumnname = referencedcolumn;
	}
	@Override
	public String toString() {
		return "ReferenceData [tablename=" + tablename + ", columname=" + columname + ", constraintname="
				+ constraintname + ", referencedtablename=" + referencedtablename + ", referencedcolumnname=" + referencedcolumnname
				+ "]";
	}
	
}
