<%@ include file="/WEB-INF/views/jsp/include.jsp"%>

<html>
<head>
<title>Students</title>
<style>
.editIcon{
  background: url(edit.png) no-repeat 6px center;
  width: 10px;
  height: 10px;
  display: block;
}
.deleteIcon{
  background: url(delete.jpg) no-repeat 6px center;
  width: 10px;
  height: 10px;
  display: block;
}
</style>
</head>
<body>
	<h3>Students</h3>
	<form:form id="studentListForm" cssClass="form-horizontal" modelAttribute="model" method="post" action="/mvc4/student/list">
		<TABLE border="1" cellspacing="1" cellpadding="1" bgColor="yellow">
			<TR>
				<TH>ID</TH>
				<TH>Name</TH>
				<TH>Age</TH>
				<TH>Sex</TH>
				<TH>Grade</TH>
				<TH>Country</TH>
				<TH>Edit</TH>
				<TH>Delete</TH>
			</TR>
			
			<c:forEach items="${model.studentList}" var="student">
				<TR>
					<TD> <c:out value="${student.rollNumber}" /> </TD>
					<TD> <c:out value="${student.name}" /> </TD>
					<TD> <c:out value="${student.age}" /> </TD>
					<TD> <c:out value="${student.sex}" /> </TD>
					<TD> <c:out value="${student.grade}" /> </TD>
					<TD> <c:out value="${student.country}" /> </TD>
					<TD><a href="edit?rollNumber=${student.rollNumber}" class="editIcon" title="Edit"></a></TD>
					<TD><a href="delete?rollNumber=${student.rollNumber}" class="deleteIcon" title="Delete"></a></TD>
				</TR>
			</c:forEach>
		</TABLE>
		<a href="<c:url value="/student/add"/>">Add Student</a>
		</form:form>
</body>

</html> 