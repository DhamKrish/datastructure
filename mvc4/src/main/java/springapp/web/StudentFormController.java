package springapp.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import springapp.domain.Student;
import springapp.service.StudentManager;



/**
 * @author dhamo
 * StudentFormController - add and list methods.
 */
@Controller
@RequestMapping("/student")
public class StudentFormController {
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private StudentManager studentManager;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView getStudentList()
            throws ServletException, IOException {

    	logger.info("Displaying Student list...");
        Map<String, Object> myModel = new HashMap<String, Object>();
        myModel.put("studentList", this.studentManager.getStudentList());
        return new ModelAndView("studentlist", "model", myModel);
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addStudent(ModelMap myModel){
    	logger.info("Calling add student");
        myModel.addAttribute("studentInfo", new StudentInfo());
    	return "studentForm";
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editStudent(@RequestParam int rollNumber, ModelMap myModel){
    	logger.info("Calling edit student for :"+rollNumber);
    	StudentInfo editStudent = this.studentManager.getStudent(rollNumber);
    	editStudent.setMode("edit");
        myModel.addAttribute("studentInfo", editStudent);
        
    	return "studentForm";
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView deleteStudent(@RequestParam int rollNumber){
    	logger.info("Calling delete student for :"+rollNumber);
    	this.studentManager.deleteStudent(rollNumber);
    	
    	return new ModelAndView("redirect:list");
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView saveStudent(@Valid StudentInfo studentInfo){
    	logger.info("Calling save student - Mode:"+studentInfo.getMode());
    	this.studentManager.saveStudentInfo(studentInfo);
    	
    	return new ModelAndView("redirect:list");
    }
    
    /*
     * Method used to populate the country list in view.
     * Note that here you can call external systems to provide real data.
     */
    @ModelAttribute("countryMap")
    public List<String> initializeCountries() {
 
        List<String> countries = new ArrayList<String>();
        countries.add("USA");
        countries.add("CANADA");
        countries.add("FRANCE");
        countries.add("GERMANY");
        countries.add("ITALY");
        countries.add("OTHER");
        return countries;
    }
    
    /*
     * Method used to populate the country list in view.
     * Note that here you can call external systems to provide real data.
     */
    @ModelAttribute("gradeMap")
    public List<String> initializeGrades() {
 
        List<String> gradesList = new ArrayList<String>();
        gradesList.add("UG");
        gradesList.add("PG");
        return gradesList;
    }
    
   // gradeMap
}
