package springapp.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springapp.domain.Student;
import springapp.repository.StudentDao;
import springapp.web.StudentInfo;

@Service("studentManager")
public class StudentManagerService implements StudentManager {

    protected final Log logger = LogFactory.getLog(getClass());
    
	@Autowired
	private StudentDao studentDao;
	
	public List<Student> getStudentList() {
		return studentDao.getStudentList();
	}

	public void saveStudentInfo(StudentInfo studentInfo) {
		Student student = new Student();
		student.setRollNumber(studentInfo.getRollNumber());
		student.setName(studentInfo.getStudentName());
		student.setAge(studentInfo.getAge());
		//TODO: To check if single char is supplied. F-Female, M-Male.
		student.setSex(studentInfo.getSex().charAt(0));
		student.setGrade(studentInfo.getGrade());
		student.setCountry(studentInfo.getCountry());
		
		logger.info("student:"+student);
		
		if("edit".equals(studentInfo.getMode())){
			studentDao.updateStudent(student);
		}else{
			studentDao.saveStudent(student);
		}
	}

	public StudentInfo getStudent(int rollNumber) {
		Student student = studentDao.getStudent(rollNumber);
		StudentInfo studentInfo = new StudentInfo();
		studentInfo.setAge(student.getAge());
		studentInfo.setCountry(student.getCountry());
		studentInfo.setGrade(student.getGrade());
		studentInfo.setRollNumber(student.getRollNumber());
		studentInfo.setSex(String.valueOf(student.getSex()));
		studentInfo.setStudentName(student.getName());
		return studentInfo;
		
	}

	public void deleteStudent(int rollNumber) {
		studentDao.deleteStudent(rollNumber);
	}

}
